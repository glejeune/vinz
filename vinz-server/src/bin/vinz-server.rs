use actix_web::{middleware, web, App, HttpServer};
use dotenv::dotenv;
use log4rs;

#[actix_rt::main]
async fn main() {
    dotenv().ok();

    let pid_file = std::env::var("SERVER_PID_FILE").unwrap_or("vinz.pid".to_string());
    vinz_server::services::utils::create_pid_file(&pid_file[..]).expect("Can't create PID file!");

    let logger_config =
        std::env::var("SERVER_LOGGER_CONFIG_FILE").expect("SERVER_LOGGER_CONFIG_FILE must be set");
    log4rs::init_file(logger_config, Default::default()).unwrap();

    let server_binding = std::env::var("SERVER_BINDING").expect("SERVER_BINDING must be set");

    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .service(
                web::scope("/api/v1")
                    .service(
                        web::resource("/databases")
                            .route(web::post().to(vinz_server::controllers::databases::register)),
                    )
                    .service(
                        web::resource("/login")
                            .route(web::post().to(vinz_server::controllers::databases::login)),
                    ),
            )
            .service(
                web::resource("/status")
                    .route(web::get().to(vinz_server::controllers::status::get_status))
                    .route(web::head().to(|| web::HttpResponse::Ok())),
            )
            .route(
                "/{filename:.*}",
                web::get().to(vinz_server::controllers::statics::index),
            )
    })
    .bind(&server_binding)
    .expect(format!("Can not bind to {}", server_binding).as_str())
    .workers(1)
    .run()
    .await
    .unwrap();
}
