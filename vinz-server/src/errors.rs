use actix_web::{error::ResponseError, HttpResponse};
use derive_more::Display;

#[derive(Debug, Display)]
pub enum ServiceError {
    #[display(fmt = "Internal Server Error")]
    InternalError,
    #[display(fmt = "Not Found")]
    NotFound,
    #[display(fmt = "Unauthorized")]
    Unauthorized,
    #[display(fmt = "Conflict")]
    Conflict,
    #[display(fmt = "Forbidden")]
    Forbidden,
    #[display(fmt = "Bad Request: {}", _0)]
    BadRequest(String),
}

#[derive(Serialize)]
struct JsonError<'a> {
    code: &'a str,
    message: &'a str,
}

impl ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServiceError::InternalError => HttpResponse::InternalServerError().json(JsonError {
                code: "IntervalServerError",
                message: "Internal Server Error, Please try later",
            }),
            ServiceError::NotFound => HttpResponse::NotFound().json(JsonError {
                code: "NotFound",
                message: "Page not found",
            }),
            ServiceError::Unauthorized => HttpResponse::Unauthorized().json(JsonError {
                code: "Unauthorized",
                message: "Can't access resource",
            }),
            ServiceError::Forbidden => HttpResponse::Forbidden().json(JsonError {
                code: "Forbidden",
                message: "Can't access resource",
            }),
            ServiceError::Conflict => HttpResponse::Conflict().json(JsonError {
                code: "Conflict",
                message: "Can't access resource",
            }),
            ServiceError::BadRequest(ref message) => HttpResponse::BadRequest().json(JsonError {
                code: "BadRequest",
                message,
            }),
        }
    }
}

impl From<jsonwebtoken::errors::Error> for ServiceError {
    fn from(error: jsonwebtoken::errors::Error) -> ServiceError {
        match error.into_kind() {
            jsonwebtoken::errors::ErrorKind::ExpiredSignature => ServiceError::Unauthorized,
            jsonwebtoken::errors::ErrorKind::Base64(_) => ServiceError::InternalError,
            jsonwebtoken::errors::ErrorKind::Json(_) => ServiceError::InternalError,
            jsonwebtoken::errors::ErrorKind::Utf8(_) => ServiceError::InternalError,
            jsonwebtoken::errors::ErrorKind::Crypto(_) => ServiceError::InternalError,
            _ => ServiceError::BadRequest("Invalid token".to_string()),
        }
    }
}

impl From<actix_http::error::PayloadError> for ServiceError {
    fn from(error: actix_http::error::PayloadError) -> ServiceError {
        match error {
            actix_http::error::PayloadError::Overflow => {
                ServiceError::BadRequest("Overflow".to_string())
            }
            actix_http::error::PayloadError::Incomplete(_) => {
                ServiceError::BadRequest("Incomplete".to_string())
            }
            actix_http::error::PayloadError::EncodingCorrupted => {
                ServiceError::BadRequest("Encoding corrupted".to_string())
            }
            actix_http::error::PayloadError::UnknownLength => {
                ServiceError::BadRequest("Unknown length".to_string())
            }
            actix_http::error::PayloadError::Http2Payload(error) => {
                ServiceError::BadRequest(error.to_string())
            }
            actix_http::error::PayloadError::Io(error) => {
                ServiceError::BadRequest(error.to_string())
            }
        }
    }
}

impl From<std::io::Error> for ServiceError {
    fn from(error: std::io::Error) -> ServiceError {
        match error.kind() {
            std::io::ErrorKind::NotFound => ServiceError::NotFound,
            std::io::ErrorKind::PermissionDenied => ServiceError::Forbidden,
            _ => ServiceError::InternalError,
        }
    }
}
