use super::errors::ServiceError;
use super::kdbx::VinzDatabase;
use chrono::Local;
use jsonwebtoken::{Header, TokenData, Validation};

lazy_static::lazy_static! {
pub static ref JWT_SECRET_KEY: String = std::env::var("JWT_SECRET_KEY").unwrap_or_else(|_| "abcd".repeat(8));
}

static ONE_WEEK: i64 = 60 * 60 * 24 * 7;

#[derive(Serialize, Deserialize, Debug)]
pub struct UserToken {
    // issued at
    pub iat: i64,
    // expiration
    pub exp: i64,
    // data
    pub email: String,
    pub database: String,
    pub uuid: String,
    pub password: String,
}

impl UserToken {
    pub fn generate_token(database: &VinzDatabase, email: &str, password: &str) -> String {
        let now = Local::now().timestamp();
        let payload = UserToken {
            iat: now,
            exp: now + ONE_WEEK,
            email: email.to_string(),
            database: database.database.name().to_string(),
            uuid: database.to_string(),
            password: password.to_string(),
        };

        jsonwebtoken::encode(
            &Header::default(),
            &payload,
            &jsonwebtoken::EncodingKey::from_secret(&JWT_SECRET_KEY.as_bytes()),
        )
        .unwrap()
    }

    pub fn decode_token(token: String) -> Result<TokenData<UserToken>, ServiceError> {
        match jsonwebtoken::decode::<UserToken>(
            &token,
            &jsonwebtoken::DecodingKey::from_secret(&JWT_SECRET_KEY.as_bytes()),
            &Validation::default(),
        ) {
            Ok(user_token) => Ok(user_token),
            Err(err) => Err(ServiceError::from(err)),
        }
    }

    // pub fn verify_token(
    //     token_data: &TokenData<UserToken>,
    //     connection: &PgConnection,
    // ) -> Result<users::User, ServiceError> {
    //     users::find_by_token(connection, &token_data.claims)
    // }
}
