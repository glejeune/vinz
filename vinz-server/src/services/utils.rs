use std::fs::File;
use std::io::prelude::*;
use std::process;

pub fn create_pid_file(path: &str) -> std::io::Result<()> {
    let mut file = File::create(path)?;
    file.write_all(process::id().to_string().as_bytes())?;
    Ok(())
}
