use derive_more::Display;
use kdbx_rs::binary::Unlocked;
use kdbx_rs::database::{Database, Entry};
use kdbx_rs::{CompositeKey, Kdbx};
use log::{debug, error};
use std::fs::File;
use std::path::{Path, PathBuf};
use uuid::Uuid;

type Result<T> = std::result::Result<T, DatabaseError>;

#[derive(Debug, Display)]
pub enum DatabaseError {
    #[display(fmt = "Failed to write database data")]
    WriteError,
    #[display(fmt = "Failed to create database file")]
    CreateFileError,
    #[display(fmt = "Failed to open database file")]
    OpenFileError,
    #[display(fmt = "Failed to unlock database")]
    UnlockError,
    #[display(fmt = "Failed to set database key")]
    SetKeyError,
    #[display(fmt = "Internal error")]
    InternalError,
}

pub struct VinzDatabase {
    pub database: Kdbx<Unlocked>,
    pub uuid: Uuid,
}

impl ToString for VinzDatabase {
    fn to_string(&self) -> String {
        self.uuid.to_hyphenated().to_string()
    }
}

pub fn new_database(email: &str, password: &str, name: &str) -> Result<VinzDatabase> {
    let mut main_database = main_database()?;
    let database = create_database(name, password, None, None)?;
    match register_database(&mut main_database, &database, email, name) {
        Ok(()) => Ok(database),
        Err(err) => Err(err),
    }
}

pub fn login_to_database(email: &str, password: &str, name: &str) -> Result<VinzDatabase> {
    let main_database = main_database()?;
    match main_database
        .database
        .find_entry(|e| e.username() == Some(email) && e.title() == Some(name))
    {
        Some(entry) => match entry.url() {
            Some(database_uuid_str) => match Uuid::parse_str(&database_uuid_str) {
                Ok(uuid) => open_database(uuid, password),
                Err(_) => Err(DatabaseError::InternalError),
            },
            None => Err(DatabaseError::InternalError),
        },
        None => Err(DatabaseError::InternalError),
    }
}

// --- private ---

fn database_path(uuid: Uuid) -> PathBuf {
    let kdbx_base_path = std::env::var("KDBX_BASE_PATH").expect("KDBX_BASE_PATH must be set");
    let mut buffer = Uuid::encode_buffer();
    let database_file = uuid.to_hyphenated().encode_lower(&mut buffer);
    let mut destination_path = PathBuf::from(&database_file);
    destination_path.set_extension("kdbx");
    Path::new(&kdbx_base_path).join(destination_path)
}

fn database_exists(uuid: Uuid) -> bool {
    let path = database_path(uuid);
    Path::new(&path).exists()
}

fn register_database(
    main_database: &mut VinzDatabase,
    database: &VinzDatabase,
    email: &str,
    name: &str,
) -> Result<()> {
    debug!("Register database {}", name);

    let mut entry = Entry::default();
    entry.set_username(email);
    entry.set_url(database.uuid.to_string());
    entry.set_title(name);

    main_database.database.add_entry(entry);

    let main_database_path = database_path(main_database.uuid);
    match File::create(main_database_path) {
        Ok(mut file) => match main_database.database.write(&mut file) {
            Ok(()) => Ok(()),
            Err(err) => {
                error!("Failed to register database {}: {}", name, err);
                Err(DatabaseError::WriteError)
            }
        },
        Err(err) => {
            error!("Failed to register database {}: {}", name, err);
            Err(DatabaseError::CreateFileError)
        }
    }
}

fn main_database() -> Result<VinzDatabase> {
    let kdbx_main_pass = std::env::var("KDBX_MAIN_PASS").expect("KDBX_MAIN_PASS must be set");
    let kdbx_main_uuid = std::env::var("KDBX_MAIN_UUID").expect("KDBX_MAIN_UUID must be set");

    match Uuid::parse_str(&kdbx_main_uuid) {
        Ok(uuid) => {
            if !database_exists(uuid) {
                create_database(
                    "Vinz",
                    &kdbx_main_pass,
                    Some(uuid),
                    Some("Vinz main database"),
                )
            } else {
                open_database(uuid, &kdbx_main_pass)
            }
        }
        Err(err) => {
            error!("Failed to access main database: {}", err);
            Err(DatabaseError::InternalError)
        }
    }
}

fn open_database(uuid: Uuid, password: &str) -> Result<VinzDatabase> {
    let database_path = database_path(uuid);
    match kdbx_rs::open(&database_path) {
        Ok(kdbx) => {
            let key = CompositeKey::from_password(&password);
            match kdbx.unlock(&key) {
                Ok(unlock) => Ok(VinzDatabase {
                    uuid: uuid,
                    database: unlock,
                }),
                Err(_) => {
                    error!("Failed to open database {}: Can't unlock database!", uuid);
                    Err(DatabaseError::UnlockError)
                }
            }
        }
        Err(err) => {
            error!("Failed to open database {}: {}", uuid, err);
            Err(DatabaseError::OpenFileError)
        }
    }
}

fn create_database(
    name: &str,
    password: &str,
    uuid: Option<Uuid>,
    description: Option<&str>,
) -> Result<VinzDatabase> {
    debug!("Create database {}", name);
    let description_db: &str = description.unwrap_or(name);
    let uuid_db = uuid.unwrap_or(Uuid::new_v4());

    let mut database = Database::default();
    database.set_name(name);
    database.set_description(description_db);
    let mut kdbx = Kdbx::from_database(database);
    match kdbx.set_key(CompositeKey::from_password(password)) {
        Ok(()) => {
            let destination_path = database_path(uuid_db);
            match File::create(destination_path) {
                Ok(mut file) => match kdbx.write(&mut file) {
                    Ok(()) => Ok(VinzDatabase {
                        uuid: uuid_db,
                        database: kdbx,
                    }),
                    Err(err) => {
                        error!("Failed to create database {}: {}", name, err);
                        Err(DatabaseError::WriteError)
                    }
                },
                Err(err) => {
                    error!("Failed to create database {}: {}", name, err);
                    Err(DatabaseError::CreateFileError)
                }
            }
        }
        Err(err) => {
            error!("Failed to create database {}: {}", name, err);
            Err(DatabaseError::SetKeyError)
        }
    }
}
