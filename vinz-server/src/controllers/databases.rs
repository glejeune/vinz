use super::errors::ServiceError;
use super::services::jwt;
use super::services::kdbx;
use actix_web::web;
use log::info;

#[derive(Deserialize)]
pub struct AccountRequest {
    email: String,
    password: String,
    database_name: String,
}

#[derive(Serialize)]
pub struct RegisterResponse {
    uuid: String,
}

#[derive(Serialize)]
pub struct AuthResponse {
    token: String,
}

// #[post("/api/v1/databases")]
pub async fn register(
    account: web::Json<AccountRequest>,
) -> Result<web::Json<RegisterResponse>, ServiceError> {
    let email = &account.email.as_str();
    let password = &account.password.as_str();
    let database_name = &account.database_name.as_str();

    match kdbx::new_database(email, password, database_name) {
        Ok(database) => {
            let response = RegisterResponse {
                uuid: database.to_string(),
            };
            info!(
                "Database {} created: {}",
                database_name,
                database.to_string()
            );
            Ok(web::Json(response))
        }
        Err(_) => Err(ServiceError::InternalError),
    }
}

// #[post("/api/v1/login")]
pub async fn login(
    account: web::Json<AccountRequest>,
) -> Result<web::Json<AuthResponse>, ServiceError> {
    let email = &account.email.as_str();
    let password = &account.password.as_str();
    let database_name = &account.database_name.as_str();

    match kdbx::login_to_database(email, password, database_name) {
        Ok(database) => {
            let token = jwt::UserToken::generate_token(&database, email, password);
            Ok(web::Json(AuthResponse { token }))
        }
        Err(_) => Err(ServiceError::Unauthorized),
    }
}
