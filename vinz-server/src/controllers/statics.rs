use actix_files;
use actix_web::{Error, HttpRequest};
use std::path::{Path, PathBuf};

pub async fn index(req: HttpRequest) -> Result<actix_files::NamedFile, Error> {
    let static_path = std::env::var("STATIC_PATH").unwrap_or_else(|_| ".".to_string());
    let mut path: PathBuf =
        Path::new(static_path.as_str()).join(req.match_info().query("filename"));
    if path.is_dir() {
        path = path.join("index.html");
    }
    let file = actix_files::NamedFile::open(path)?;
    Ok(file.use_last_modified(true))
}
