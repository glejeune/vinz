pub async fn get_status() -> &'static str {
    "ok\r\n"
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::executor::block_on;

    #[test]
    fn test_index_ok() {
        let resp = block_on(get_status());
        assert_eq!(resp, "ok\r\n");
    }
}
