# Vinz

At the beginning was Vinz Clortho...

> Vinz Clortho the Keymaster of Gozer was a demigod and loyal minion of The Destructor.

Then I created Vinz, a simple kdbx server.

:warning: This project is a WIP, DO NOT USE IT IN PRODUCTION !!!

## Install

Install Rust and NodeJS, then run :

```
npm run setup
npm run build
npm run start
```

Finally, open http://localhost:7455

## API

### Register

Create a new database.

```
curl -v -X POST -H "Content-Type: application/json" --data '{"email":"john.doe@example.com","password":"xyz", "database_name":"sample"}' http://localhost:7455/api/v1/databases
```

### Login

Login

```
curl -v -X POST -H "Content-Type: application/json" --data '{"email":"john.doe@example.com","password":"xyz", "database_name":"sample"}' http://localhost:7455/api/v1/login
```

## Dev

`npm` commands:

- `npm run build` : Build server and app.
- `npm run build:release` : Build server and app for production.
- `npm run build:server` : Build Vinz server.
- `npm run build:server:release` : Build Vinz server for production.
- `npm run build:app` : Build Vinz front app.
- `npm run build:app:release` : Build Vinz front app for production.
- `npm run build:js` : Build front app JS bundles.
- `npm run build:js:esm` : Build front app JS esm bundles.
- `npm run build:js:iife` : Build front app JS iife bundles.
- `npm run clean` : Clean build.
- `npm run setup` : Setup project.
- `npm run start` : Start server.
