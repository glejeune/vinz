import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import { terser } from "rollup-plugin-terser";
import copy from "rollup-plugin-copy";
import { minify } from "html-minifier";

const copyIndexHTML = {
  src: "vinz-app/static/index.html",
  dest: "static",
  transform: (content) =>
    minify(content.toString(), {
      collapseWhitespace: true,
      removeEmptyAttributes: true,
      removeComments: true,
    }),
};

const iife = {
  input: "vinz-app/static/main.js",
  output: {
    format: "iife",
    file: "static/vinz.iife.min.js",
    name: "vinz",
  },
  plugins: [
    commonjs(),
    resolve(),
    babel(),
    terser(),
    copy({
      targets: [
        {
          src: "vinz-app/static/vinz_bg.wasm",
          dest: "static",
          rename: "vinz.iife.min_bg.wasm",
        },
        copyIndexHTML,
      ],
    }),
  ],
};

const esm = {
  input: "vinz-app/static/main.js",
  output: {
    format: "es",
    file: "static/vinz.esm.min.js",
  },
  plugins: [
    commonjs(),
    resolve(),
    babel(),
    terser(),
    copy({
      targets: [
        {
          src: "vinz-app/static/vinz_bg.wasm",
          dest: "static",
          rename: "vinz.esm.min_bg.wasm",
        },
        copyIndexHTML,
      ],
    }),
  ],
};

const conf = process.env.BABEL_ENV === "esm" ? esm : iife;
export default conf;
